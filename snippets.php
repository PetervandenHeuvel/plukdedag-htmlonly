
<!-- This is de header -->
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--head -->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php wp_title(); ?>
    </title>
    <meta name="viewport" content="width=device-width" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>
</head>
<!--einde head -->


<!-- Opening Body -->
<body <?php body_class(); ?>>


<!-- Header -->
<header id="masthead" class="site-header" role="banner">
    <hgroup>
        <div class="site-branding">
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
        </div>
    </hgroup>
    <!-- menu + menu-toggle-->
    <nav id="site-navigation" class="main-navigation" role="navigation">
        <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button>
        <?php wp_nav_menu( $args ); ?>
    </nav>
    <!-- einde menu  -->
</header>
<!-- einde header -->


<!-- The 'WordPress loop' -->

    <!-- the loop -->
    <?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
    <div class="post">
        <h1><a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
            </a></h1>
        <?php the_time( 'l, F jS, Y') ?>
        <div class="entry">
            <?php the_post_thumbnail(); ?>
            <?php the_content(); ?>
            <p class="postmetadata">
                <?php _e( 'Filed under&#58;'); ?>
                <?php the_category( ', ') ?>
                <?php _e( 'by'); ?>
                <?php the_author(); ?>
                <br />
                <?php comments_popup_link( 'No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
                <?php edit_post_link( 'Edit', ' &#124; ', ''); ?>
            </p>
            <br />
        </div>
    </div>
    <?php endwhile; ?>
    <div class="navigation">
        <?php posts_nav_link(); ?>
    </div>
    <?php endif; ?>
    <!-- einde the loop -->


<!-- de sidebar -->

<!-- sidebar -->
<aside id="secondary" class="widget-area" role="complementary">
    <ul>
        <?php if ( function_exists( 'dynamic_sidebar') && dynamic_sidebar() ) : else : ?>
        <?php endif; ?>
    </ul>
</aside>
<!-- einde sidebar  -->


<!-- footer -->
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="site-info">
        <?php bloginfo( 'name'); print " - "; echo date( 'Y'); ?>
    </div>
</footer>
<!-- einde footer -->